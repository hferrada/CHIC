all: externals index 

externals:
	@echo "Compiling external tools"
	@cd ext;./compile.sh;cd ..;

index:
	@cd src;make;cd ..;

clean:
	@ cd src;make clean;cd ..;
	@ cd ext;./clean.sh;cd ..;
