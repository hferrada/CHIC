#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./config.sh"
#N_CHUNKS=10
N_CHUNKS=20  
# ch=30 will test feature to allow chunks shorter than ref.
N_THREADS=48

## TODO: run it on all 5,6 ,7, 8 
INPUT_FILE=${CHICO_SRC}/test_scripts/bio_data/data_8/genome.fa
#INPUT_FILE="/home/scratch-hdd/dvalenzu/data/PG_PREPROCESSOR_DATA/all_fastas/ALL_CHRS/allChrs.201Refs.fasta"  # 500 GB, parallelism sucks
#INPUT_FILE="/home/scratch-hdd/dvalenzu/data/PG_PREPROCESSOR_DATA/all_fastas/CHR1/samp3G.fasta"
#INPUT_FILE="/home/scratch-hdd/dvalenzu/data/PG_PREPROCESSOR_DATA/all_fastas/CHR1/Chr1.2001Refs.fasta" # 500 GB, paralellism ok
#INPUT_FILE="/home/scratch-hdd/dvalenzu/data/PG_PREPROCESSOR_DATA/all_fastas/CHR1/Chr1.1Refs.fasta"
#INPUT_FILE="/home/scratch-hdd/dvalenzu/data/PG_PREPROCESSOR_DATA/all_fastas/ALL_CHRS/samp_50G.fasta"  # 50GB, ??


MAX_MEM_MB=200000
# NEEDS TO BE AS LARGE AS THE INPUT FILE.
# OTHERWISE, IT IS VERY LIKELY TO CRASH.
DECODE_BUFFER_SIZE=10000
## The decode buffer needs to be sligthly larger than the ref.

REF_SIZE_MB=6000
#REF_SIZE_MB=10

CODER_BIN=./rlz_parser.sh
DECODER_BIN=../../LZ-Decoder/decode
utils_assert_file_exists ${CODER_BIN} 
utils_assert_file_exists ${DECODER_BIN} 

COMPRESSED_FILE=tmp.rlz_compressed
DECOMPRESSED_FILE=tmp.reconstructed
rm -f ${COMPRESSED_FILE}
rm -f ${DECOMPRESSED_FILE}
rm -f ${INPUT_FILE}.*

echo "Encoding..."
${CODER_BIN} ${INPUT_FILE} ${COMPRESSED_FILE} ${REF_SIZE_MB} ${N_CHUNKS} ${N_THREADS} ${MAX_MEM_MB}
echo "Decoding..."
${DECODER_BIN}  ${COMPRESSED_FILE} ${DECOMPRESSED_FILE} ${DECODE_BUFFER_SIZE}
echo "Verifying (diff)..."
#cmp ${DECOMPRESSED_FILE} ${INPUT_FILE}

TMP_FILE=tmp.output
utils_fasta_to_plain ${INPUT_FILE} ${TMP_FILE}
#vimdiff ${DECOMPRESSED_FILE} ${TMP_FILE}
cmp ${DECOMPRESSED_FILE} ${TMP_FILE}
echo "vimdiff ${DECOMPRESSED_FILE} ${TMP_FILE}"


utils_success_exit
